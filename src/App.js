import React from 'react';
import AppNavBar from './components/AppNavBar';
import Banner from './components/Banner';
import Cards from './components/Cards';
import { Container, Row, Col } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

// import './App.css';

const App = () => {
  return (
    <>
      <AppNavBar />
      <Container fluid>
        <Row>
          <Col lg={{ span: 6, offset: 3 }}>
            <Banner />
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 6, offset: 3 }} className="d-flex justify-content-between align-items-center">

            <Cards 
            title="Learn from Home"
            description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Maecenas pharetra convallis posuere morbi leo. Commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend. Id faucibus nisl tincidunt eget nullam non nisi. Eget nunc lobortis mattis aliquam faucibus purus in massa."
            />

            <Cards 
            title="Study Now Pay Later"
            description="Lectus arcu bibendum at varius. Malesuada proin libero nunc consequat interdum varius sit amet mattis. Sit amet aliquam id diam maecenas ultricies. Nulla pellentesque dignissim enim sit amet venenatis urna cursus eget. Elit at imperdiet dui accumsan sit amet nulla facilisi. Arcu cursus vitae congue mauris. Mattis enim ut tellus elementum sagittis vitae et. Felis eget velit aliquet sagittis id consectetur purus ut. Nunc eget lorem dolor sed viverra ipsum nunc."
            />

            <Cards 
            title="Be Part Of Our Community"
            description="Urna et pharetra pharetra massa massa. Proin libero nunc consequat interdum varius sit. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Quam nulla porttitor massa id. Cras sed felis eget velit aliquet sagittis. Orci phasellus egestas tellus rutrum tellus pellentesque eu. Maecenas pharetra convallis posuere morbi leo urna molestie. Aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices. Enim sed faucibus turpis in eu mi bibendum."
            />
          </Col>
        </Row>      
      </Container>
    </>
  )
}

export default App;