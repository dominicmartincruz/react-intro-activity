import React from 'react';
import { Card, Button } from 'react-bootstrap'

export default function Cards({title, description}) {
    return (
        <Card style={{ width: '25rem', maxHeight: '20rem', minHeight: '20rem'}}>
            {/* <Card.Img variant="top" src="holder.js/100px180" /> */}
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                    {description}
                </Card.Text>
                {/* <Button variant="primary">Go somewhere</Button> */}
            </Card.Body>
        </Card>
    )
}

