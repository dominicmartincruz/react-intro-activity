import React from 'react';
import { Navbar, Nav, NavDropdown, Form, Button, FormControl } from 'react-bootstrap'

export default function AppNavBar() {
    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#home">Courses</Nav.Link>
                    <Nav.Link href="#link">Login</Nav.Link>
                    <Nav.Link href="#link">Register</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

